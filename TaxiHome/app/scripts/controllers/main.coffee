'use strict'

app = angular.module('taxiHomeApp')
app.controller 'MainCtrl', ($scope, STRSService) ->
  $scope.showModal = STRSService.activateModal
  $scope.bookings = STRSService.getBookings()
