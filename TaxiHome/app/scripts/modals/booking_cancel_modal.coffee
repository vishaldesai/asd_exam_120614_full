'use strict'

app = angular.module('taxiHomeApp')

app.controller 'AsyncNotifModalController', ($scope, STRSService) ->
  booking = STRSService.getBooking()
  $scope.customer_name = booking.customer_name
  $scope.pickup_address = booking.pickup_address
  $scope.phone = booking.phone

  $scope.accept = ->
    STRSService.notifyDecision('ACCEPTED')
    booking.confirmation_time = Time.now()

  $scope.reject = ->
    STRSService.notifyDecision('REJECTED')

app.factory 'AsyncNotifModal', (btfModal) ->
  btfModal {
    controller: 'AsyncNotifModalController'
    controllerAs: 'modal'
    templateUrl: 'views/cancelled.html'
  }
