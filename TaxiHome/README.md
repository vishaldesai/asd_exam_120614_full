# Setting up the project

Of course, you should start by installing all the required libraries and node packages. Use the following commands:

```sh
bower install
npm install
```

Do not forget to properly configure `Pusher`. To this end, edit the file `app/scripts/services/strs_service.coffee`.
