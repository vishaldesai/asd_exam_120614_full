'use strict'

app = angular.module('taxiClientApp')
app.controller 'MainCtrl', ($scope, BookingsSyncService, BookingsAsyncService) ->
  $scope.name = ''
  $scope.phone = ''
  $scope.pickupAddress = ''
  $scope.syncNotification = ''
  $scope.asyncNotification = ''

  $scope.submit = ->
    BookingsSyncService.save {
      'customer_name': $scope.name
      'phone': $scope.phone
      'pickup_address': $scope.pickupAddress
    }, (data) ->
      $scope.syncNotification = data

  $scope.$on 'async_notification', ->
    $scope.$apply ->
      $scope.asyncNotification = BookingsAsyncService.getAsyncNotification()
