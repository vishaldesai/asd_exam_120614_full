'use strict'

app = angular.module('taxiClientApp')

app.service 'BookingsSyncService', ($rootScope, $http) ->
  lastBookingId = 0

  save: (bookingDetails, callback) ->
    $http.post('http://localhost:3000/bookings', {booking: bookingDetails})
      .success (data, status) ->
        lastBookingId = data.bookingId
        callback( data.message )
  getLastBookingId: ->
    lastBookingId


app.service 'BookingsAsyncService', ($rootScope) ->
  asyncNotification = ''

  pusher = new Pusher('3c1ea30ba63adcca183e')
  channel = pusher.subscribe('bookings')
  channel.bind 'async_notification', (data) ->
    asyncNotification = data.message
    $rootScope.$broadcast 'async_notification'

  getAsyncNotification: ->
    asyncNotification

