Then(/^I should eventually receive a cancellation confirmation with pending payment$/) do
  expect(@customer.find_by_id('async_notification').text).to eq('Booking has been cancelled')
end

When(/^I cancel the booking$/) do
  @customer.click_button('Cancel')
end


