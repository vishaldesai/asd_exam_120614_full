Feature: Taxi booking cancellation
  As a customer
  So that I can cancel a booking
  I want to see the cancellation confirmation and pending payment

  @javascript
  Scenario: Before Taxi driver confirms
    Given I have access to STRS via its web application
    And I submit a booking request
    When I cancel the booking
    Then I should eventually receive a cancellation confirmation with pending payment

  Scenario: After Taxi driver confirms
    Given I have access to STRS via its web application
    And I submit a booking request
    And the taxi driver accepts the request
    When I cancel the booking
    Then I should eventually receive a cancellation confirmation with pending payment