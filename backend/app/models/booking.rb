class Booking < ActiveRecord::Base
  attr_accessible :customer_name, :phone, :pickup_address, :pickup_time, :status ,:confirmation_time

  belongs_to :taxi
end
