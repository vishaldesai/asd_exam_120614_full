class BookingsController < ApplicationController
  def create
    b = Booking.create(params[:booking])
    TaxiAllocatorJob.new.async.perform(b.id)
    render :json => {:message => "Booking is being processed"}, :status => :created
  end

  def updateTaxiAssignment
    b = params[:booking]
    booking = Booking.find(b[:id])
    booking.update_attributes status: b[:status]

    if b[:status].eql? 'REJECTED'
      BookingRejection.create(:booking_id => booking.id, :taxi_id => booking.taxi_id)
      TaxiAllocatorJob.new.async.perform(b.id)
    else
      Pusher.trigger('bookings','async_notification', {:message => 'Your taxi will arrive soon'}.to_json)
    end
    render :nothing => true
  end

  def cancel
    b = params[:booking]
    booking = Booking.find(b[:id])
    booking.update_attributes status: b[:status]
    price = TaxiAllocatorJob.estimate_cancel_price(booking)
    Pusher.trigger('bookings','async_notification', {:message => 'Your booking has been cancelled.'}.to_json)
  end
end