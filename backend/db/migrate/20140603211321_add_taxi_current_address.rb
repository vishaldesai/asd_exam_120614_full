class AddTaxiCurrentAddress < ActiveRecord::Migration
  def change
    add_column :taxis, :current_address, :string
  end
end
