class CreateBookings < ActiveRecord::Migration
  def change
    create_table :bookings do |t|
      t.string :customer_name
      t.string :phone
      t.string :pickup_address
      t.time :pickup_time
      t.string :status
      t.belongs_to :taxi

      t.timestamps
    end

    create_table :taxis do |t|
      t.string :driver_name

      t.timestamps
    end
  end
end
